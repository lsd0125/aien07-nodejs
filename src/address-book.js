const express = require('express');
const db = require(__dirname + '/__connect_db');
const moment = require('moment-timezone');

const router = express.Router();

// middleware
router.use((req, res, next)=>{
    res.locals.title = '通訊錄';

    const whiteList = ['list', 'login'];
    console.log(req.url.split('/'));
    let u = req.url.split('/')[1];

    if( whiteList.indexOf(u) < 0){
        // 如果是 edit, del ...
        if(res.locals.isAdmin){
            return next();
        } else {
            return res.redirect('/address-book/list');
        }
    }
    next();
});


router.get('/add', (req, res)=>{
    res.locals.title = '新增資料 - 通訊錄';
    res.locals.pageName = 'ab-add';
    res.render('address-book/add');
});

router.post('/add', (req, res)=>{
    // TODO: 檢查資料格式, 是否必填

    const output = {
        success: false,
        postData: req.body,
    };
    const sql = "INSERT INTO `address_book`(`name`, `email`, `mobile`, `birthday`, `address`, `created_at`) VALUES " +
        "(?, ?, ?, ?, ?, NOW())";
    db.query(sql, [
        req.body.name,
        req.body.email,
        req.body.mobile,
        req.body.birthday,
        req.body.address,
    ], (error, results)=>{
        if(error){
            console.log(error);
            output.error = error;
        } else {
            output.success = true;
            output.results = results;
        }
        res.json(output);
    })
});

router.post('/add2', (req, res)=>{
    // TODO: 檢查資料格式, 是否必填
    req.body.created_at = new Date();
    const sql = "INSERT INTO `address_book` SET ?";
    db.query(sql,  req.body , (error, results)=>{
        if(error){
            console.log(error);
            res.json({
                postData: req.body,
                error: error
            });
        } else {
            res.json({
                postData: req.body,
                results: results
            });
        }
    })
});

router.get('/del/:sid', (req, res)=>{
    const sql = "DELETE FROM `address_book` WHERE `sid`=?";
    db.query(sql, [req.params.sid], (error, results)=>{
        console.log('del:', results);
        //res.json(results);
        //res.send(req.header('Referer'));
        res.redirect(req.header('Referer')); // 回到原來的頁面
    })
});

router.get('/edit/:sid', (req, res)=>{
    const sql = "SELECT * FROM `address_book` WHERE `sid`=?";
    db.query(sql, [req.params.sid], (error, results)=>{
        if(results && results.length===1){
            //res.json(results);
            results[0].birthday = moment(results[0].birthday).format('YYYY-MM-DD');
            res.render('address-book/edit', {row: results[0]});

        } else {
            return res.redirect('/address-book/list');
        }
    })
});

router.post('/edit/:sid', async (req, res)=>{
    // TODO: 可以先判斷資料是否符合規則
    const output = {
        success: false,
        info: '',
        postData: req.body,
    };

    const s_sql = "SELECT 1 FROM `address_book` WHERE email=? AND sid <> ? ";
    const r1 = await db.queryAsync(s_sql, [req.body.email, req.params.sid]);

    if(r1.length){
        output.info = 'Email 資料重複';
        return res.json(output);
    }

    const sql = "UPDATE `address_book` SET ? WHERE sid=?";
    const r2 = await db.queryAsync(sql, [req.body, req.params.sid]);

    if(r2.changedRows === 1){
        output.info = '編輯成功';
        output.success = true;
    } else {
        output.info = '資料沒有變更';
    }
    res.json(output);
});

router.post('/edit-old2/:sid', (req, res)=>{
    // TODO: 可以先判斷資料是否符合規則
    const output = {
        success: false,
        info: '',
        postData: req.body,
    };

    const s_sql = "SELECT 1 FROM `address_book` WHERE email=? AND sid <> ? ";
    db.queryAsync(s_sql, [req.body.email, req.params.sid])
        .then(results=>{
            if(results.length){
                output.info = 'Email 資料重複';
                res.json(output);
            } else {
                const sql = "UPDATE `address_book` SET ? WHERE sid=?"
                return  db.queryAsync(sql, [req.body, req.params.sid]);
            }
        })
        .then(results=>{
            if(!results) return;
            if(results.changedRows === 1){
                output.info = '編輯成功';
                output.success = true;
            } else {
                output.info = '資料沒有變更';
            }
            res.json(output);
        })
        .catch(error=>{
            output.info = '更新資料錯誤';
            output.error = error;
            res.json(output);
        })

});

router.post('/edit-old1/:sid', (req, res)=>{
    // TODO: 可以先判斷資料是否符合規則
    const output = {
        success: false,
        info: '',
        postData: req.body,
    };

    const s_sql = "SELECT 1 FROM `address_book` WHERE email=? AND sid<>? ";
    db.query(s_sql, [req.body.email, req.params.sid], (error, results)=> {
        if(results.length){
            output.info = 'Email 資料重複';
            res.json(output);
        } else {
            const sql = "UPDATE `address_book` SET ? WHERE sid=?"
            db.query(sql, [req.body, req.params.sid], (error, results)=>{
                if(error){
                    output.info = '更新資料錯誤';
                    output.error = error;
                    res.json(output);
                } else {
                    if(results.changedRows === 1){
                        output.info = '編輯成功';
                        output.success = true;
                    } else {
                        output.info = '資料沒有變更';
                    }
                    res.json(output);
                }
            })
        }
    });
});

router.get('/fake', (req, res)=>{
    return res.send('off');
    const sql = "INSERT INTO `address_book`(`name`, `email`, `mobile`, `birthday`, `address`, `created_at`) VALUES " +
        "(?, ?, ?, ?, ?, NOW())";

    for(let i=0; i<100; i++){
        let email = new Date().getTime().toString(16) + Math.floor(Math.random()*1000) + '@gmail.com';
        db.query(sql, [
            '陳小華',
            email,
            '0918123456',
            '1990-11-12',
            '台北市',
        ]);
    }
    res.send('ok');
});

router.get('/list/:page?', (req, res)=>{
    res.locals.title = '列表 - 通訊錄';
    res.locals.pageName = 'ab-list';
    const perPage = 5;
    let totalRows = 0;
    let totalPages = 0;
    let page = req.params.page ? parseInt(req.params.page) : 1;

    const t_sql = "SELECT COUNT(1) num FROM address_book";
    db.query(t_sql, (error, results)=>{
        totalRows = results[0].num; // 總筆數

        // TODO: 要排除沒資料的狀況
        totalPages = Math.ceil(totalRows/perPage);  // 總頁數
        if(page<1){
            res.redirect('/address-book/list/1');
            return;
        }
        if(page>totalPages){
            res.redirect('/address-book/list/'+totalPages);
            return;
        }
        const sql = `SELECT * FROM address_book ORDER BY sid DESC LIMIT ${(page-1)*perPage}, ${perPage}`;

        db.query(sql, (error, results)=>{
            const fm = 'YYYY-MM-DD';
            results.forEach(function(el){
                el.birthday = moment(el.birthday).format(fm);
                // el.birthday = moment(el.birthday).tz('Europe/London').format('YYYY-MM-DD HH:mm:ss');
            });

            const output = {
                perPage,
                page,
                totalRows,
                totalPages,
                rows: results
            };
            if(res.locals.isAdmin){
                res.render('address-book/list-admin', output);
            } else {
                res.render('address-book/list', output);
            }
        });
    });

});

router.get('/login', (req, res)=>{
    res.render('address-book/login');
});
router.post('/login', (req, res)=>{
    const sql = "SELECT sid, `account`, `nickname` FROM `admins` WHERE `account`=? AND `password`=SHA1(?)";
    db.queryAsync(sql, [req.body.account, req.body.password])
        .then(results=>{
            if(results && results.length===1){
                req.session.admin = results[0];
                res.json({
                    success: true,
                    admin: results[0]
                });
            } else {
                res.json({success: false});
            }
        })
        .catch(error=>{
            res.json({
                success: false,
                error: error
            });
        })
});
router.get('/logout', (req, res)=>{
    delete req.session.admin;
    res.redirect('address-book/login');
});


module.exports = router;