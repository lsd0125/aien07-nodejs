let f1 = a=>a*a;

let f2 = ()=>{
    let t = 0;
    for(let i=1; i<=10; i++){
      t+=i;
    }
    return t;
};

console.log( f1(7));
console.log(f2());