const express = require('express');
console.log('test: ', express.my_name123456);
const router = express.Router();

router.get('/admin2/:p1?/:p2?', (req, res)=>{
    res.json({
        params: req.params,
        url: req.url,
        baseUrl: req.baseUrl
    });
});

module.exports = router;