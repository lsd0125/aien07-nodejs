const express = require('express');
express.my_name123456 = 'shiner';

const session = require('express-session');
const cors = require('cors');
const axios = require('axios');
const cheerio = require('cheerio');

const multer = require('multer');
const fs = require('fs');
const upload = multer({dest:'tmp_uploads'});

const app = express();

app.set('view engine', 'ejs');


// 解析 post urlencoded 的 middleware
app.use(express.urlencoded({extended: false}));

// 解析 json 的 middleware
app.use(express.json());

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'sdfg799@djfhk',
    cookie: {
        maxAge: 1200000
    }
}));
app.use(cors());

// 自訂 middleware
app.use((req, res, next)=>{
    res.locals.pageName = '';
    res.locals.isAdmin = false;

    req.query.from_middleware = 'hello';
    if(req.session.admin && req.session.admin.account){
        res.locals.admin = req.session.admin;
        res.locals.isAdmin = true;
    }
    next();
});

const db = require(__dirname + '/__connect_db');

app.get('/', (req, res)=>{
    res.render('home', {name:'david'});
});
app.get('/abc', (req, res)=>{
    res.send('aaaaaaaabbc');
});

app.get('/json-file', (req, res)=>{
    const sales = require(__dirname + '/../data/sales');
    res.render('sales', {sales: sales});
});

app.get('/try-qs', (req, res)=>{
    res.json(req.query);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});

app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);
});

app.post('/try-post-json', (req, res)=>{
    res.json(req.body);
});

app.post('/try-plus', (req, res)=>{
     let a = req.body.a*1 || 0;
     let b = req.body.b*1 || 0;
    res.send(a+b+'');
});

app.post('/try-plus2', (req, res)=>{
    const output = {
        postData: req.body
    };
    let a = req.body.a*1 || 0;
    let b = req.body.b*1 || 0;
    output.result = a+b;

    res.json(output);
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    let ext = '';
    switch(req.file.mimetype){
        case 'image/png':
            ext = '.png';
            break;
        case 'image/jpeg':
            ext = '.jpg';
    }
    if(ext){
        let filename = (new Date().getTime()) + ext;
        fs.rename(
            req.file.path,
            './public/img/' + filename,
            error=>{
                res.json({
                    success: true,
                    img: '/img/'+filename,
                    body: req.body
                });
            }
        )
    } else {
        fs.unlink(req.file.path, eeror=>{
            res.json({
                success: false,
                info: 'bad file type'
            });
        })
    }
});

app.get('/my-params1/aa/bb', (req, res)=>{
    res.json({ok: 'ok'});
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/mobile\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res)=>{
    let u = req.url.split('?')[0];
    u = u.split('-').join('');
    let m = u.slice(8);
    res.json({
        url: req.url,
        mobile: m
    });
});

// const admin2 = require(__dirname + '/admins/admin2');
// app.use(admin2);
app.use('/my-path', require(__dirname + '/admins/admin2') );

app.get('/try-session', (req, res)=>{
    req.session.my_var = req.session.my_var || 0;
    req.session.rand = req.session.rand || Math.floor(Math.random()*10000);
    req.session.my_var ++;
    res.json({
        my_var: req.session.my_var,
        session: req.session
    });
});

app.get('/try-db', (req, res)=>{
    const sql = "SELECT * FROM address_book LIMIT 0,5";

    db.query(sql, (error, results, fields)=>{
        console.log(fields);

        res.json(results);
    });
});

app.get('/try-axios', (req, res)=>{
    axios.get('https://tw.yahoo.com/')
        .then(response=>{
            let $ = cheerio.load(response.data);
            let str = '';
            $('.MainStoryImage').each(function(i,el){
                let src = $(this).attr('src');
                str += `<img src="${src}"><br>`;
            })

            res.send( str );
        })
});

app.get('/just-pending', (req, res)=>{

});

app.use('/address-book', require(__dirname + '/address-book'));

app.use(express.static('public'));

app.use((req, res)=>{
    // res.type('text/html');
    res.status(404);
    res.send(`
    <h2>Page Not Found !!!!!!!!!!!!!</h2>
    `);
});


app.listen(3000, ()=>{
   console.log('server started!');
});
